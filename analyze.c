/****************************************************/
/* File: analyze.c                                  */
/* Semantic analyzer implementation                 */
/* for the TINY compiler                            */
/* Compiler Construction: Principles and Practice   */
/* Kenneth C. Louden                                */
/****************************************************/

#include "globals.h"
#include "symtab.h"
#include "analyze.h"

static int isMainDeclared = FALSE;
static int returnMatch = 0;
static int flagParam = 0;
static char* currentFunctionName;
int functioncheck(TreeNode *);
int funcvaluecheck(TreeNode *, TreeNode *);


/* counter for variable memory locations */
static int location = 0;
TreeNode *root;
/* Procedure traverse is a generic recursive 
 * syntax tree traversal routine:
 * it applies preProc in preorder and postProc 
 * in postorder to tree pointed to by t
 */
static void traverse( TreeNode * t,
               void (* preProc) (TreeNode *),
               void (* postProc) (TreeNode *) )
{ if (t != NULL)
  { preProc(t);
    { int i;
      for (i=0; i < MAXCHILDREN; i++)
        traverse(t->child[i],preProc,postProc);
    }
    postProc(t);
    traverse(t->sibling,preProc,postProc);
  }
}

static void scopeError(TreeNode * t, char * message)
{
	fprintf(listing, "Scope error at line %d: %s\n", t->lineno, message);
	Error = TRUE;
}

static void typeError(TreeNode * t, char * message)
{ fprintf(listing,"Type error at line %d: %s\n",t->lineno,message);
  Error = TRUE;
}

/* nullProc is a do-nothing procedure to 
 * generate preorder-only or postorder-only
 * traversals from traverse
 */
static void nullProc(TreeNode * t)
{ if (t==NULL) return;
  else return;
}

/* Procedure insertNode inserts 
 * identifiers stored in t into 
 * the symbol table 
 */
static void insertNode( TreeNode * t)
{
	LOG("	> insertNode [%d]\n", t->nodekind);
	switch (t->nodekind)
	{
		case StmtK:
			switch (t->kind.stmt)
			{
				case ReturnK:
					st_lookup(currentFunctionName, t);
					if (returnMatch == 2)
					{
						returnMatch = 0;
					}
					break;
				//compound statement start
				case CompoundK:
					returnMatch++;
					if (flagParam)
					{
						flagParam = FALSE;
					}
					else
					{
						location = enterScope(location);
					}
					break;
				//compound statement end
				case CompoundEndK:
					returnMatch--;
					location = exitScope(location);
					if (returnMatch == 1)
					{
						flagParam = FALSE;
					}
					break;
				//call
				case CallK:
					//if function is not in symbol table
					if (st_lookup(t -> attr.name, t) == -1)
					{
						scopeError(t, "Undeclared function");
					}
					else
					{
						st_insert(t -> attr.name, t->lineno, 0, TRUE, t->type, FALSE, -1);
						t -> isFunction = TRUE;
					}
					break;
				default:
					break;
/*				case AssignK:
				case ReadK:

					if (st_lookup(t->attr.name) == -1)
						/ * not yet in table, so treat as new definition * /
						st_insert(t->attr.name,t->lineno,location++);
    					else
    						/ * already in table, so ignore location, 
    						   add line number of use only * / 
    						st_insert(t->attr.name,t->lineno,0);
						break;
				default:
					break;*/
			}
			break;
		case ExpK:
			switch (t->kind.exp)
			{
				case IdK:
					//if this id is not in symbol table;
					if (st_lookup(t -> attr.name, t) == -1)
					{
						scopeError(t, "Undeclared variable");
					}
					else
					{
						st_insert(t->attr.name, t->lineno, 0, FALSE, t->type, FALSE, t->size);
					}
					break;
				case ParamListK:
					flagParam = TRUE;
					location = enterScope(location);
					break;
				default:
					break;
/*
					if (st_lookup(t->attr.name) == -1)
						/ * not yet in table, so treat as new definition * /
						st_insert(t->attr.name,t->lineno,location++);
					else
						/ * already in table, so ignore location, 
						   add line number of use only * / 
        				st_insert(t->attr.name,t->lineno,0);
        			break;
					
				default:
					break;
					*/
			}
			break;
		case DecK:
			switch (t -> kind.dec)
			{
				//Variable and parameter declaration
				case VariableK:
				case ParameterK:
					if (t -> attr.name != NULL &&
						t -> attr.name[0] != '\0')
					{
						//when variable is not declared in 'out' and 'in' scope(not in both symtab);
						if (st_lookup(t -> attr.name, t) == -1 || st_dec_lookup(t -> attr.name, t) == -1)
						{
							st_insert(t -> attr.name, t -> lineno, location++, FALSE, t -> type, TRUE, t->size);
						}
						else
						{
							scopeError(t, "Variable is already declared in this scope");
						}
					}
					break;
				//Function declaration
				case FunctionK:
					//If main function is already declared before this function
					if (isMainDeclared)
					{
						scopeError(t, "Main function should be the last function to be declared");
					}
					
					//if this is main function, mark isMainDeclared flag to true
					if (strcmp(t -> attr.name, "main") == -0)
					{
						isMainDeclared = TRUE;
					}

					//if this function is not in symtab
					if (st_lookup(t -> attr.name, t) == -1)
					{
						currentFunctionName = t->attr.name;
						st_insert(t -> attr.name, t -> lineno, location++, TRUE, t -> type, TRUE, -1);
						t -> isFunction = TRUE;
						if (t -> type == Integer)
						{
							returnMatch = 1;
						}
					}
					else if (st_dec_lookup(t -> attr.name, t) == -1)
					{
						currentFunctionName = t -> attr.name;
						st_insert(t -> attr.name, t -> lineno, location++, TRUE, t -> type, TRUE, -1);
						t -> isFunction = TRUE;
					}
					//if this function is already declared
					else
					{
						scopeError(t, "Function is already declared");
					}
					break;
				default:
					break;
			}
		default:
			break;
  }
}

/* Function buildSymtab constructs the symbol 
 * table by preorder traversal of the syntax tree
 */
void buildSymtab(TreeNode * syntaxTree)
{ traverse(syntaxTree,insertNode,nullProc);
  if (TraceAnalyze)
  { fprintf(listing,"\nSymbol table:\n\n");
    printSymTab(listing);
  }
}



/* Procedure checkNode performs
 * type checking at a single tree node
 */
/*

static void checkNode(TreeNode * t)
{ 
	printf("chchchnode ------------------ %s, %d, %d\n", t->attr.name, t->nodekind, t->kind);
	switch (t->nodekind)
  { 
		case DecK:
			switch (t->kind.dec){
							case VariableK:
							if(t->type == Void)
											typeError(t->child[0], "Variable must be 'Integer type'.");
											break;
							case ParameterK:
							if(t->type == Void)
											typeError(t->child[0], "Parameter must not be 'Void type'.");
											break;
							case FunctionK:
							if(strcmp(t->attr.name, "main")==0){
											if(t->type != Void)
															typeError(t, "main function must be 'Void type'.");
											if(t->child[1]->child[0] != NULL)
											{
															typeError(t->child[1], "Main function must not have parameter.");
															break;
											}
											if(t->child[1]->type!=Void)
															typeError(t->child[1], "Main function must not have parameter.");
							}
							break;
							default:
							break;
			}
			break;

			case ExpK:
			switch (t->kind.exp)
			{
							case IdK:
							if(t->type == Array){
											if(t->child[0] != NULL){
															if(t->type!=Integer && t->kind.exp != OpK)
																			typeError(t->child[0], "Array's index must be 'Integer type'.");
											}
							}
							if(t->type == Integer){
											if(t->child[0] != NULL)
															typeError(t->child[0], "This is not Array variable.");
							}
							break;

							case OpK:
							if(t->child[0]->kind.exp != OpK && t->child[1]->kind.exp != OpK && t->child[0]->type != Integer && t->child[0]->type != Array && t->child[1]->type != Integer && t->child[1]->type != Array)
							{
											typeError(t->child[0], "Void type can't be calculated.");
							}
							break;
			}
			break;

			case StmtK:
			switch(t->kind.stmt)
			{
							case IfK:
							if(t->child[0]->type != Integer && t->child[0]->kind.exp != OpK)
											typeError(t->child[0], "If statement must be 'Integer type'.");
											break;
							case AssignK:
							if(t->child[0]->type == Integer || t->child[0]->type == Array)
											if(t->child[1]->type == Integer || t->child[1]->type == Array || t->child[1]->kind.exp == OpK)
															break;
							else
											typeError(t->child[0], "Assign statement must be 'Integer value'.");
											break;

							case WhileK:
							if(t->child[0]->type != Integer && t->child[0]->kind.exp != OpK)
											typeError(t->child[0], "While statement must be 'Integer type'.");
											break;

							case ReturnK:
							if(t->type == Void){
											typeError (t->child[0], "Void type must not have return value.");
							}
							else if(t->type == Integer){
											if(t->child[0] != NULL){
															if(t->type != t->child[0]->type)
																			typeError(t->child[0], "Function type and return type must be same.");
											}
							}
							break;

							case CallK:
							if(t->isFunction != 1)
											typeError(t, "Not function!");
							else
											if(functioncheck(t) == -1)
															typeError(t, "Parameter doesn't match.");
					
							break;
							default:
							break;
}	
break;
	default:
	break;
 } 
}
*/

static void checkNode(TreeNode * t)
 {
  switch (t->nodekind)
   { case ExpK:
       switch (t->kind.exp)
       { case OpK:
			 		if(t->child[0]->kind.exp !=OpK && t-> child[1]->kind.exp != OpK && t->child[0]->type!=Integer && t->child[0]->type!=Array  && t->child[1]->type!=Integer && t->child[1]->type!=Array)
					{
									typeError(t->child[0], "Void type variable can't be calculated.");
					}
					break;
         case ConstK: break;
         case IdK:
				 	if(t->type == Array) {
									if(t->child[0] != NULL)
									{
													if(t->child[0]->type!=Integer&&t->child[0]->kind.exp != OpK)
																	typeError(t->child[0], "Array's index must be 'Integer type'.");
									}
					}
					if(t->type == Integer){
									if(t->child[0] != NULL)
													typeError(t->child[0], "This is Not Array type variable.");
					}
					break;
				 case TypeK:break;
				 case ParamListK:break;
				 case ArgumentListK:break;
         default:
           break;
       }
       break;
     case StmtK:
       switch (t->kind.stmt)
       { case IfK:
			 		if(t->child[0]->type != Integer && t->child[0]->kind.exp != OpK)
									typeError(t->child[0], "If statement must be 'Integer type'.");
				 break;
				 case ElseK: break;
				 case WhileK:
				 	if(t->child[0]->type != Integer && t->child[0]->kind.exp != OpK)
									typeError(t->child[0], "While statement must be 'Integer type'.");
					break;
				 case CompoundK: break;
				 case ReturnK:
				 	if(t->type == Void)
									typeError(t->child[0], "Void type function must not have return value.");
				  else if(t->type == Integer){
									if(t->child[0]!=NULL){
													if(t->type != t->child[0]->type)
																	typeError(t->child[0], "Function type and Return type are not same.");
									}
					}
					break;
				 case AssignK:
				 	if (t->child[0]->type == Integer || t->child[0]->type == Array)
					{				if(t->child[1]->type == Integer || t->child[1]->type == Array || t->child[1]->kind.exp == OpK)
													break;
					}
					else
					{
									typeError(t->child[0], "Assign statement must be integer value.");
					}
					break;
				 case CallK:
				 	if(t->isFunction!=1)
									typeError(t, "Not function!");
					else
						if(functioncheck(t) == -1)
										typeError(t, "Parameter doesn't match with Caller.");
					break;
				 case CompoundEndK:break;
         default:
           break;
       }
       break;
		 case DecK:
			switch (t->kind.dec)
			{
				case ParameterK:
					if(t->type == Void)
									typeError(t->child[0], "Parameter must not be 'Void type'.");
					break;
				case FunctionK:
					if(strcmp(t->attr.name,"main")==0){
									if(t->type != Void)
													typeError(t, "Main function must be 'Void type.");
									if(t->child[1]->child[0] != NULL)
									{
													typeError(t->child[1], "Main function must not have parameter.");
												  break;
									}
									if(t->child[1]->type!=Void)
													typeError(t->child[1], "Main function must not have parameter.");
					}
					break;
				case VariableK:
					if(t->type == Void)
								typeError(t->child[0], "Variable must be 'Integer type'.");
					break;
				default:
					break;
			}
		break;
     default:
       break;

   }
 }

/* Procedure typeCheck performs type checking 
 * by a postorder syntax tree traversal
 */
void typeCheck(TreeNode * syntaxTree)
{ 
				static int flag = 0;
				if(!flag){
								root = syntaxTree;
								flag = 1;
				}
				traverse(syntaxTree,nullProc,checkNode);
}

int functioncheck(TreeNode *call_node)
{
				TreeNode * declaration = root;

				char * tmp;
				tmp = call_node->attr.name;
				while(TRUE){
								if(declaration != NULL){
												if(declaration->kind.dec == FunctionK){
																if(!strcmp(declaration->attr.name, call_node->attr.name)){
																				declaration = declaration->child[1];
																				call_node = call_node->child[0];
																				if(call_node == NULL){
																								if(declaration->child[0] == NULL)
																												return 0;
																								return -1;
																				}
																				else
																				{
																								if(functioncheckvoid(tmp)){
																												if(declaration->child[0] == NULL)
																																return 0;
																												return -1;
																								}
																								else{
																												if(declaration->child[0] == NULL)
																																return -1;
																								}
																				}

																				declaration = declaration->child[0];
																				call_node = call_node->child[0];
																				while(1)
																				{
																								if(call_node == NULL && declaration == NULL)
																												return 0;
																								else if(funcvaluecheck(declaration, call_node) == -1)
																												return -1;

																								call_node = call_node->sibling;
																								declaration = declaration->sibling;
																				}
																}
																else
																				declaration = declaration->sibling;
												}
												else
																declaration = declaration->sibling;
								}
								else
												return -1;
				}
}

int funcvaluecheck(TreeNode *call_node, TreeNode *declaration_node)
{
				if(declaration_node == NULL || call_node == NULL)
								return -1;
				return 0;
}

int functioncheckvoid(char *name)
{
				TreeNode * declaration_node = root;

				while(TRUE){
								if(declaration_node != NULL){
												if(declaration_node->kind.dec == FunctionK)
												{
																if(!strcmp(declaration_node->attr.name, name)){
																				if(declaration_node->type == VOID)
																				{
																								return TRUE;
																				}
																				else
																				{
																								return FALSE;
																				}
																}
												}
												declaration_node = declaration_node->sibling;
								}
								else
												return FALSE;
				}
}
