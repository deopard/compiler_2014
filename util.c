/****************************************************/
/* File: util.c                                     */
/* Utility function implementation                  */
/* for the TINY compiler                            */
/* Compiler Construction: Principles and Practice   */
/* Kenneth C. Louden                                */
/****************************************************/

#include "globals.h"
#include "util.h"
//#define PRINT_RESERVE(TOKEN) fprintf(listing, "%10d%15s%10s%5s\n", lineno, TOKEN, " ", tokenString);
#define PRINT_RESERVE(TOKEN) fprintf(listing, "%s  %s\n", TOKEN, tokenString);
/* Procedure printToken prints a token 
 * and its lexeme to the listing file
 */
int c_name = 0;
void printToken( TokenType token, const char* tokenString )
{
	switch (token)
	{
/*		case STRING:
			PRINT_RESERVE("STRING")
			break;*/
		case IF:
			PRINT_RESERVE("IF")
			break;
		case THEN:
			PRINT_RESERVE("THEN")
			break;
		case ELSE:
			PRINT_RESERVE("ELSE")
			break;
/*		case END:
			PRINT_RESERVE("END")
			break;
		case REPEAT:
			PRINT_RESERVE("REPEAT")
			break;
		case UNTIL:
			PRINT_RESERVE("UNTIL")
			break;
		case READ:
			PRINT_RESERVE("READ")
			break;
		case WRITE:
			PRINT_RESERVE("WRITE")
			break;*/
		case INT:
			PRINT_RESERVE("INT")
			break;
		case VOID:
			PRINT_RESERVE("VOID")
			break;
/*		case DO:
			PRINT_RESERVE("DO")
			break;
		case WHILE:
			PRINT_RESERVE("WHILE")
			break;
		case FOR:
			PRINT_RESERVE("FOR")
			break;*/
		case RETURN:
			PRINT_RESERVE("RETURN")
			break;
		case EQ:
			PRINT_RESERVE("EQ		=")
			break;
		case LT:
			PRINT_RESERVE("LT		<")
			break;
		case LE:
			PRINT_RESERVE("LE		<=")
			break;
		case GT:
			PRINT_RESERVE("GT		>")
			break;
		case GE:
			PRINT_RESERVE("GE		>=")
			break;
		case EQUAL:
			PRINT_RESERVE("EQUAL	==")
			break;
/*		case NE:
			PRINT_RESERVE("NE")
			break;*/
		case LPAREN:
			PRINT_RESERVE("LPAREN")
			break;
		case RPAREN:
			PRINT_RESERVE("RPAREN")
			break;
/*		case LBRACE:
			PRINT_RESERVE("LBRACE")
			break;
		case RBRACE:
			PRINT_RESERVE("RBRACE")
			break;
		case LBRACKET:
			PRINT_RESERVE("LBRACKET")
			break;
		case RBRACKET:
			PRINT_RESERVE("RBRACKET")
			break;
		case SEMI:
			PRINT_RESERVE("SEMI")
			break;*/
		case PLUS:
			PRINT_RESERVE("PLUS		+")
			break;
		case MINUS:
			PRINT_RESERVE("MINUS	-")
			break;
		case TIMES:
			PRINT_RESERVE("TIMES	*")
			break;
		case COMMA:
			PRINT_RESERVE("COMMA")
			break;
		case OVER:
			PRINT_RESERVE("OVER		/")
			break;
		case ENDFILE:
			PRINT_RESERVE("ENDFILE")
			break;
		case NUM:
			PRINT_RESERVE("NUM")
			break;
		case ID:
			PRINT_RESERVE("ID")
			break;
		case ERROR:
			PRINT_RESERVE("ERROR")
			break;
/*		case COMMENTERROR:
			PRINT_RESERVE("COMMENT ERROR")
//			fprintf(listing, "%16s%10s%5s\n", "***", " ", "invalid comment");
			break;*/
		default: /* should never happen */
			fprintf(listing,"Unknown token: %d\n",token);
	}
}

/* Function newStmtNode creates a new statement
 * node for syntax tree construction
 */
TreeNode * newStmtNode(StmtKind kind)
{ TreeNode * t = (TreeNode *) malloc(sizeof(TreeNode));
  int i;
  if (t==NULL)
    fprintf(listing,"Out of memory error at line %d\n",lineno);
  else {
    for (i=0;i<MAXCHILDREN;i++) t->child[i] = NULL;
    t->sibling = NULL;
    t->nodekind = StmtK;
    t->kind.stmt = kind;
    t->lineno = lineno;
	t->type = Default;
	t->isFunction = FALSE;
  }
  return t;
}

/* Function newExpNode creates a new expression 
 * node for syntax tree construction
 */
TreeNode * newExpNode(ExpKind kind)
{ TreeNode * t = (TreeNode *) malloc(sizeof(TreeNode));
  int i;
  if (t==NULL)
    fprintf(listing,"Out of memory error at line %d\n",lineno);
  else {
    for (i=0;i<MAXCHILDREN;i++) t->child[i] = NULL;
    t->sibling = NULL;
    t->nodekind = ExpK;
    t->kind.exp = kind;
    t->lineno = lineno;
    t->type = Default;
	t->isFunction = FALSE;
  }
  return t;
}

TreeNode * newDecNode(DecKind kind)
{
	TreeNode * t = (TreeNode *)malloc(sizeof(TreeNode));
	int i;
	if (t == NULL)
	{
		fprintf(listing, "Out of memory error at line %d\n", lineno);
	}
	else
	{
		for (i=0; i<MAXCHILDREN; i++)
			t->child[i] = NULL;
		t->sibling = NULL;
		t->nodekind = DecK;
		t->kind.exp = kind;
		t->lineno = lineno;
		t->type = Default;
		t->size = 0;
		t->isFunction = FALSE;
	}

	return t;
}

/* Function copyString allocates and makes a new
 * copy of an existing string
 */
char * copyString(char * s)
{ int n;
  char * t;
  if (s==NULL) return NULL;
  n = strlen(s)+1;
  t = malloc(n);
  if (t==NULL)
    fprintf(listing,"Out of memory error at line %d\n",lineno);
  else strcpy(t,s);
  return t;
}

/* Variable indentno is used by printTree to
 * store current number of spaces to indent
 */
static int indentno = 0;

/* macros to increase/decrease indentation */
#define INDENT indentno+=2
#define UNINDENT indentno-=2

/* printSpaces indents by printing spaces */
static void printSpaces(void)
{ int i;
  for (i=0;i<indentno;i++)
    fprintf(listing," ");
}

/* procedure printTree prints a syntax tree to the 
 * listing file using indentation to indicate subtrees
 */
void printTree( TreeNode * tree )
{ int i;
  INDENT;

	/*printf("========STMT======\n");
	printf("Ifk			%d\n", IfK);
	printf("Whilek		%d\n", WhileK);
	printf("Elsek		%d\n", ElseK);
	printf("Compoundk	%d\n", CompoundK);
	printf("Returnk		%d\n", ReturnK);
	printf("AssignK		%d\n", AssignK);*/

  while (tree != NULL) {
    printSpaces();
    if (tree->nodekind==StmtK)
    {
		switch (tree->kind.stmt) {

        case IfK:
          fprintf(listing,"If\n");
          break;
		case WhileK:
		  fprintf(listing,"While\n");
		  break;
/*        case RepeatK:
          fprintf(listing,"Repeat\n");
          break;*/
		case ElseK:
		  fprintf(listing, "Else\n");
		  break;
		case CompoundK:
		  fprintf(listing, "Compound Statement\n");
		  break;
		case ReturnK:
		  fprintf(listing, "Return\n");
		  break;
        case AssignK:
          fprintf(listing,"Assign to: %s\n",tree->attr.name);
          break;
/*        case ReadK:
          fprintf(listing,"Read: %s\n",tree->attr.name);
          break;
        case WriteK:
          fprintf(listing,"Write\n");
          break;*/
		case CallK:
			fprintf(listing,"Function Call Exptression: %s\n",tree->attr.name);
			break;
		case CompoundEndK:
			fprintf(listing, "\n");
			break;
	 
		default:
          fprintf(listing,"Unknown StmtNode kind %d %c\n", tree->kind.stmt, tree->kind.stmt);
          break;
		}
    }
    else if (tree->nodekind==ExpK)
    {
		switch (tree->kind.exp) {
        case OpK:
          fprintf(listing,"Op: ");
          printToken(tree->attr.op,"\0");
          break;
        case ConstK:
          fprintf(listing,"Const: %d\n",tree->attr.val);
          break;
        case IdK:
          fprintf(listing,"A Variable Expression:%s\n",tree->attr.name);
          break;
/*        case AdditiveK:
          fprintf(listing,"Additive Expression\n");
          break;
       case TermK:
          fprintf(listing,"Term\n");
          break;
       case VarK:
          fprintf(listing,"Simple Expression : %s\n",tree->attr.name);
          break;
      case SimpleK:
          fprintf(listing,"Simple Expression\n");
          break;
       case ArrayK:
          fprintf(listing,"Array : %s\n", tree->attr.name);
          break;
	   case FuncK:
          fprintf(listing,"Function Declared : %s\t",tree->attr.name);
          break;*/
	   case ParamListK:
		  fprintf(listing,"Parameter List\n");
		  break;
	   case ArgumentListK:
		  fprintf(listing,"Argument List\n");
		  break;
	   default:
          fprintf(listing,"Unknown ExpNode kind %d %c\n", tree->kind.exp, tree->kind.exp);
          break;
      }
    }
	else if (tree->nodekind==DecK)
	{
		switch(tree -> kind.dec)
		{
/*			case ParameterArrK:
				fprintf(listing, "Array Parameter : %s\n", tree -> attr.name);
				break;
			case ArrK:
				fprintf(listing, "Array Declared : %s , Index Size : %d\n", tree -> attr.name, tree -> childIndex);
				break;
			case ParamVoidK:
				fprintf(listing, "Void Parameter\n");
				break;
			case ParameterDecK:
				fprintf(listing, "Parameter Lists\n");
				break; */
			case TypeK:
				fprintf(listing, "%s Type\n", tree -> attr.name);
				break;
			case ParameterK:
				fprintf(listing, "A Variable Parameter : %s\n", tree -> attr.name);
				break;
			case FunctionK:
				fprintf(listing, "A Function Declared : %s\n", tree -> attr.name);
				break;
			case VariableK:
				fprintf(listing, "A Variable %s\n", tree -> attr.name);
				break;
			default:
				fprintf(listing, "Unknown DecNode kind\n");
		}
	}
    else fprintf(listing,"Unknown node kind\n");
    for (i=0;i<MAXCHILDREN;i++)
         printTree(tree->child[i]);
    tree = tree->sibling;
  }
  UNINDENT;
}
