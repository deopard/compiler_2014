#
# makefile for TINY
# Borland C Version
# K. Louden 2/3/98
#

CC = gcc

CFLAGS = -Wall

LFLAGS =

LEX = flex

BISON = bison

YFLAGS = -d -v

#OBJS = main.o util.o scan.o parse.o symtab.o analyze.o code.o cgen.o
OBJS = main.o util.o project3_6.tab.o lex.yy.o symtab.o analyze.o code.o

project3_6: $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o project3_6

#main.o: main.c globals.h util.h scan.h parse.h analyze.h cgen.h lex.yy.c
#	$(CC) $(CFLAGS) -c main.c

main.o: main.c globals.h util.h project3_6.tab.h lex.yy.c
	$(CC) $(CFLAGS) -c main.c

util.o: util.c util.h globals.h project3_6.tab.h
	$(CC) $(CFLAGS) -c util.c

lex.yy.o: lex.yy.c scan.h util.h globals.h
	$(CC) $(CFLAGS) -Wno-unused-function -c lex.yy.c

project3_6.tab.o: project3_6.tab.h project3_6.tab.c
	$(CC) $(CFLAG) -c project3_6.tab.c

project3_6.tab.h project3_6.tab.c: project3_6.y
	$(BISON) $(YFLAGS) project3_6.y

lex.yy.c: project3_6.l
	$(LEX) $(LFLAGS) project3_6.l

#project3_6.tab.c: 6.y
#	$(BISON) -d project3_6.y

#project3_6.tab.h: 6.y
#	$(BISON) -d project3_6.y

#scan.o: scan.c scan.h util.h globals.h
#	$(CC) $(CFLAGS) -Wno-unused-function -c scan.c

#scan.c: project3_6.l
#	$(LEX) $(LFLAGS) -o scan.c project3_6.l

#parse.o: parse.c parse.h scan.h globals.h util.h
#	$(CC) $(CFLAGS) -ffreestanding -c  parse.c

symtab.o: symtab.c symtab.h
	$(CC) -g $(CFLAGS) -c symtab.c

analyze.o: analyze.c globals.h symtab.h analyze.h
	$(CC) -g $(CFLAGS) -c analyze.c

code.o: code.c code.h globals.h
	$(CC) $(CFLAGS) -c code.c

#cgen.o: cgen.c globals.h symtab.h code.h cgen.h
#	$(CC) $(CFLAGS) -c cgen.c

#mainflex.o: main.c util.o project3_6.tab.o lex.yy.o
#	$(CC) $(CFLAGS) -DFLEX_ONLY -c main.c -o mainflex.o

#FLEXOBJS = mainflex.o util.o project3_6.tab.o lex.yy.o
#flex-only: $(FLEXOBJS)
#	$(CC) $(CFLAGS) $(FLEXOBJS) -o project3_6-flex

clean:
	-rm project3_6
	-rm lex.yy.c
	-rm project3_6.tab.c
	-rm project3_6.tab.h
	-rm project3_6.output
	-rm $(OBJS)
#	-rm mainflex.o
#	-rm $(FLEXOBJS)

all: project3_6

