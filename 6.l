/****************************************************/
/* File: tiny.l                                     */
/* Lex specification for TINY                       */
/* Compiler Construction: Principles and Practice   */
/* Kenneth C. Louden                                */
/****************************************************/

%{
#include "globals.h"
#include "util.h"
#include "scan.h"
#include "6.tab.h"

/* lexeme of identifier or reserved word */
char tokenString[MAXTOKENLEN+1];
%}

digit       [0-9]
number      {digit}+
letter      [a-zA-Z]
identifier  {letter}+
newline     \n
newline2	\r
whitespace  [ \t]+

%%
<<EOF>>			{return ENDFILE;}
"int"			{return INT;}
";"             {return SEMI;}
"/*"			{ 
					char c = ' ';
					for (;;) {
						while (
								(c = input()) != '*' 
								&& c != EOF
						)
						{
							if (c == '\n' || c == '\r')
								lineno++;
						}
						if (c == EOF) {
							printf("comment ERROR\n");
							break;
							//return COMMENTERROR;
						}
						else if (c == '*')
						{
							while((c=input()) == '*') {}
							if (c == '\n' || c == '\r')
								lineno++;
							if (c == '/')
								break;
						}
					}
				}
"*/"			{return ERROR;}

"void"			{return VOID;}
"if"            {return IF;}
"then"          {return THEN;}
"else"          {return ELSE;}
"while"			{return WHILE;}
"return"		{return RETURN;}
"=="            {return EQUAL;}
"!="			{return NE;}
"<="			{return LE;}
"<"             {return LT;}
">="			{return GE;}
">"				{return GT;}
"="				{return EQ;}
"+"             {return PLUS;}
"-"             {return MINUS;}
","				{return COMMA;}
"("             {return LPAREN;}
")"             {return RPAREN;}
"{"				{return LBRACE;}
"}"				{return RBRACE;}
"["				{return LBRACKET;}
"]"				{return RBRACKET;}
{number}        {val = atoi(yytext); return NUM;}
{identifier}    {
					c_name++;
					strcpy(name[c_name], yytext);
					return ID;
				}
{newline}       {lineno++;}
{newline2}		{lineno++;}
{whitespace}    {/* skip whitespace */}
"*"             {return TIMES;}
"/"             {return OVER;}
.               {return ERROR;}

%%

void initParser(void) {
	lineno++;
	yyin = source;
	yyout= listing;
}

int yywrap(void) {
	return 1;
}

TokenType getToken(void)
{ static int firstTime = TRUE;
  TokenType currentToken;
  if (firstTime)
  { firstTime = FALSE;
    lineno++;
    yyin = source;
    yyout = listing;
  }
  currentToken = yylex();
  strncpy(tokenString,yytext,MAXTOKENLEN);
  if (TraceScan) {
    fprintf(listing,"\t%d: ",lineno);
    printToken(currentToken,tokenString);
  }
  return currentToken;
}



/*
\".*\"			{return STRING;}
"void"			{return VOID;}
"int"			{return INT;}
"if"            {return IF;}
"then"          {return THEN;}
"else"          {return ELSE;}
"end"           {return END;}
"repeat"        {return REPEAT;}
"until"         {return UNTIL;}
"read"          {return READ;}
"write"         {return WRITE;}
"do"			{return DO;}
"for"			{return FOR;}
"while"			{return WHILE;}
"return"		{return RETURN;}
"=="            {return EQ;}
"!="			{return NE;}
"<="			{return LE;}
"<"             {return LT;}
">="			{return GE;}
">"				{return GT;}
"="				{return ASSIGN;}
"+"             {return PLUS;}
"-"             {return MINUS;}
","				{return COMMA;}
"("             {return LPAREN;}
")"             {return RPAREN;}
"{"				{return LBRACE;}
"}"				{return RBRACE;}
"["				{return LBRACKET;}
"]"				{return RBRACKET;}
";"             {return SEMI;}
{number}        {return NUM;}
{identifier}    {return ID;}
{newline}       {lineno++;}
{newline2}		{lineno++;}
{whitespace}    {/ * skip whitespace * /}
"/ *"			{
					char c = ' ';
					for (;;) {
						while (
								(c = input()) != '*' 
								&& c != EOF
						)
						{
							if (c == '\n' || c == '\r')
								lineno++;
						}
						if (c == EOF)
							return COMMENTERROR;
						else if (c == '*')
						{
							while((c=input()) == '*') {}
							if (c == '\n' || c == '\r')
								lineno++;
							if (c == '/')
								break;
						}
					}
				}
"* /"			{return ERROR;}
"*"             {return TIMES;}
"/"             {return OVER;}
<<EOF>>			{return ENDFILE;}
.               {return ERROR;}

*/
