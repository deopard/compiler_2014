%{
#define YYPARSER
#include <stdio.h>
#include "globals.h"
#include "util.h"
#include "scan.h"


#define YYSTYPE TreeNode *
static char savedFuncName[100][1024];
static char savedName[100];
static int savedLineNo;
static TreeNode *savedTree;
char tokenString[MAXTOKENLEN+1];
int yyerror(char *message);
TreeNode * parse(void);
extern int c_name;
extern char name[100][1024];
%}

%token IF THEN ELSE WHILE RETURN VOID INT ENDFILE                                                                    
%token NUM ID ERROR
%token SEMI LBRACKET RBRACKET LPAREN RPAREN NE PLUS LT MINUS TIMES OVER EQUAL LE GE EQ COMMA GT LBRACE RBRACE
%left PLUS MINUS TIMES OVER LT LE GT GE EQ NE COMMA
%right ASSIGN
%start program 

%%

program				: declaration_list
					{
						savedTree = $1;
					}
					;
declaration_list	: declaration_list declaration
					{
						YYSTYPE t = $1;
						if (t != NULL)
						{
							while (t->sibling != NULL)
							{
								t=t->sibling;
							}
							t->sibling = $2;
							$$ = $1;
						}
						else
						{
							$$ = $2;
						}
					}
					| declaration
					{
						$$ = $1;
					}
					;
declaration			: var_declaration
					{
						$$ = $1;
					}
					| fun_declaration
					{
						$$ = $1;
					}
					;
var_declaration		: type_specifier ID SEMI
					{
						$$ = newDecNode(VariableK);
						$$ -> type = $1 -> type;
						$$ -> child[0] = $1;
						//$$ -> childIndex = 1;

						//savedLineNo = lineno;
						//strcpy($$ -> attr.name, tokenString);
						//strcpy($$ -> attr.name, name[c_name]);

						$$ -> attr.name = copyString(tokenStringTwo);
						//c_name--;
					}
					| type_specifier id LBRACKET NUM RBRACKET SEMI
					{
						$$ = newDecNode(VariableK);
						$$ -> type = Array;
						if (strcmp(tokenStringTwo, "") == 0)
						{
							printf("tokenStringTwo is empty\n");
							$$ -> size = -1;
						}
						else
						{
							$$ -> size = atoi(tokenStringTwo);
						}
						//strcpy($$ -> attr.name, $2 -> attr.name);
						
						//$$ = newDecNode(ArrK);
						
						//int i, a = atoi(copyString(tokenString));
						/*for (i=0; i<a; i++)
						{
							TreeNode *t = newDecNode(ArrK);
							t -> lineno = savedLineNo;
							$$ -> child[i] = t;
							$$ -> attr.name = savedName;
						}*/

						$$ -> child[0] = $1;

						$$ -> attr.name = $2 -> attr.name;
						/*$$ -> childIndex = val;
						//strcpy($$ -> attr.name, tokenString);
						strcpy($$->attr.name, name[c_name]);
						c_name--;*/
						
					}
					;
id					: ID
					{
						$$ = newExpNode(IdK);
						//strcpy($$ -> attr.name, copyString(tokenString));
						$$ -> attr.name = copyString(tokenStringTwo);
					}
					;
type_specifier		: INT
					{
						/*$$ = newDecNode(Integer);
						$$ -> attr.type = INT;
						$$ -> lineno = lineno;
						$$ -> childIndex = 0;*/
						$$ = newExpNode(TypeK);
						$$ -> type = Integer;
						//strcpy($$ -> attr.name, "int");
					}
					| VOID
					{
						/*$$ = newDecNode(Void);
						$$ -> attr.type = VOID;
						$$ -> lineno = lineno;
						$$ -> childIndex = 0;*/
						$$ = newExpNode(TypeK);
						$$ -> type = Void;
						//strcpy($$ -> attr.name, "void");
					}
					;	
fun_declaration		: type_specifier id	LPAREN params RPAREN compound_stmt
					{
						$$ = newDecNode(FunctionK);
						//strcpy($$ -> attr.name, savedFuncName);
						//$$ -> childIndex = 3;
						$$ -> lineno = $1 -> lineno;
						$$ -> type = $1 -> type;
						$$ -> child[0] = $1;
						//strcpy($$ -> attr.name, name[c_name]);
						//c_name--;

						$$ -> child[1] = $4;

						$$ -> child[2] = $6;

						$$ -> attr.name = $2 -> attr.name;
					}
					;
params				: param_list
					{
						//$$ = newDecNode(ParameterDecK);
						$$ = newExpNode(ParamListK);
						$$ -> child[0] = $1;
						//$$ -> childIndex = 1;
					}
					| VOID
					{
						//$$ = newDecNode(ParamVoidK);
						$$ = newDecNode(ParamListK);
						$$ -> type = Void;
						$$ -> attr.name = NULL;
						//$$ -> childIndex = 0;
					}
					;
param_list			:param_list COMMA param
					{
						YYSTYPE t = $1;
						if (t != NULL)
						{
							while(t -> sibling != NULL)
							{
								t = t-> sibling;
							}
							t -> sibling = $3;
							$$ = $1;
						}
						else
						{
							$$ = $3;
						}
					}
					| param
					{
						$$ = $1;
					}
					;
param				: type_specifier ID
					{
						$$ = newDecNode(ParameterK);
						$$ -> type = $1 -> type;
						$$ -> child[0] = $1;
						//$$ -> childIndex = 1;

						//strcpy($$ -> attr.name, tokenString);
						//savedLineNo = lineno;
						//strcpy($$ -> attr.name, name[c_name]);
						//c_name--;

						$$ -> attr.name = copyString(tokenStringTwo);
					}
					| type_specifier ID LBRACKET RBRACKET
					{
						//$$ = newDecNode(ParameterArrK);
						$$ = newDecNode(ParameterK);
						$$ -> type = Array;
						$$ -> child[0] = $1;
						$$ -> size = -1;
						//strcpy($$ -> attr.name, tokenString);
						//savedLineNo = lineno;
						//strcpy($$ -> attr.name, name[c_name]);
						//c_name--;
						$$ -> attr.name = copyString(tokenStringTwo);
					}
					;
compound_stmt		: LBRACE local_declarations statement_list RBRACE
					{
						$$ = newStmtNode(CompoundK);
						$$ -> child[0] = $2;
						$$ -> child[1] = $3;
						$$ -> child[2] = newStmtNode(CompoundEndK);
						//$$ -> childIndex = 2;
					}
					;
local_declarations	: local_declarations var_declaration
					{
						YYSTYPE t = $1;
						if (t != NULL)
						{
							while(t -> sibling != NULL)
							{
								t = t -> sibling;	
							}
							t -> sibling = $2;
							$$ = $1;
						}
						else
						{
							$$ = $2;
						}
					}
					| 
					{
						$$ = NULL;
					}
					;	
statement_list		: statement_list statement
					{
						YYSTYPE t = $1;
						if (t != NULL)
						{
							while (t -> sibling != NULL)
							{
								t = t -> sibling;
							}
							t -> sibling = $2;
							$$ = $1;
						}
						else
						{
							$$ = $2;
						}
					}
					|
					{
						$$ = NULL;
					}
					;
statement			: expression_stmt
					{
						$$ = $1;
					}
					| compound_stmt
					{
						$$ = $1;
					}
					| selection_stmt
					{
						$$ = $1;
					}
					| iteration_stmt
					{
						$$ = $1;
					}
					| return_stmt
					{
						$$ = $1;
					}
					;
expression_stmt		: expression SEMI
					{
						$$ = $1;
					}
					| SEMI
					;
selection_stmt		: IF LPAREN expression RPAREN statement
					{ 
						$$ = newStmtNode(IfK);
						$$ -> child[0] = $3;
						$$ -> child[1] = $5;
					}
					|
					IF LPAREN expression RPAREN statement ELSE statement
					{
						$$ = newStmtNode(IfK);
						$$ -> child[0] = $3;
						$$ -> child[1] = $5;
						if ($$ -> sibling == NULL)
						{
							$$ -> sibling = newStmtNode(ElseK);
							$$ -> sibling -> child[0] = $7;
						}
						else
						{
							YYSTYPE t = $$ -> sibling;
							$$ -> sibling = newStmtNode(ElseK);
							$$ -> sibling -> child[0] = $7;
							$$ -> sibling -> sibling = t;
						}
						/*$6 = newStmtNode(ElseK);
						$$ -> child[2] = $6;
						$$ -> child[3] = $7;*/
					}
					;
iteration_stmt		: WHILE LPAREN expression RPAREN compound_stmt
					{ 
						$$ = newStmtNode(WhileK);
						$$ -> child[0] = $3;
						$$ -> child[1] = $5;
					}
					;
return_stmt			: RETURN SEMI
					{
						$$ = newStmtNode(ReturnK);
						$$ -> type = Void;
					}
					| RETURN expression SEMI
					{  
						$$ = newStmtNode(ReturnK);
						$$ -> type = Integer;
						$$ -> child[0] = $2;
					}
					;
expression			: var EQ expression
					{
						$$ = newStmtNode(AssignK);
						$$ -> child[0] = $1;
						$$ -> child[1] = $3;

						/*$$ = newExpNode(VarK); 
						$$ -> child[0] = $1;
						$$ -> child[1] = newExpNode(OpK);
						$$ -> child[1] -> attr.op = EQ;
						$$ -> child[2] = $3;*/
					}
					| simple_expression
					{ 
						$$ = $1;
						/*$$ = newExpNode(SimpleK);
						$$ -> child[0] = $1;*/
					}

					;
var					: ID
					{
						$$ = newExpNode(IdK);
						$$ -> type = Integer;
						$$ -> attr.name = copyString(tokenStringTwo);
						//strcpy($$ -> attr.name, tokenString);
						/*strcpy($$ -> attr.name, name[c_name]);
						c_name--;*/
					}
					| id LBRACKET expression RBRACKET
					{ 
						$$ = newExpNode(IdK);
						$$ -> type = Array;
						$$ -> child[0] = $3;
						/*
						$$ = newExpNode(ArrayK);
						$$ -> child[0] = $4;
						*/
						//strcpy($$ -> attr.name, tokenString);
						/*strcpy($$ -> attr.name, name[c_name]);
						c_name--;*/
						$$ -> attr.name = $1 -> attr.name;
					}
					;
simple_expression	: additive_expression relop additive_expression
					{ 
						/*$$ = newExpNode(SimpleK);		
						$$ -> child[0] = $1;
						$$ -> child[1] = $2;
						$$ -> child[2] = $3;*/
						$$ = $2;
						$$ -> child[0] = $1;
						$$ -> child[1] = $3;
					}
					| additive_expression
					{ 
						$$ = $1;
					}
					;
relop				: LT
					{  
						$$ = newExpNode(OpK);
						$$ -> attr.op = LT;
					}
					| GT
					{
						$$ = newExpNode(OpK);
						$$ -> attr.op = GT;
					}

					| LE
					{ 
						$$ = newExpNode(OpK);
						$$ -> attr.op = LE;
					}
					| EQUAL
					{ 
						$$ = newExpNode(OpK);
						$$ -> attr.op = EQUAL;
					}
					| NE
					{
						$$ = newExpNode(OpK);
						$$ -> attr.op = NE;
					}
					| GE
					{
						$$ = newExpNode(OpK);
						$$ -> attr.op = GE;
					}
					;
additive_expression	: additive_expression addop term
					{
					/*	$$ = newExpNode(AdditiveK);

						$$ -> child[0] = $1;
						$$ -> child[1] = $2;
						$$ -> child[2] = $3;*/
						$$ = $2;
						$$ -> child[0] = $1;
						$$ -> child[1] = $3;
					}
					| term
					{
						$$ = $1;
					}
					;
addop				: MINUS
					{  
						$$ = newExpNode(OpK);
						$$ -> attr.op = MINUS;
					}
					| PLUS 
					{ 
						$$ = newExpNode(OpK);
						$$ -> attr.op = PLUS;
					}
					;
term				: term mulop factor
					{
						/*$$ = newExpNode(TermK);
						$$ -> child[0] = $1;
						$$ -> child[1] = $2;
						$$ -> child[2] = $3;*/
						$$ = $2;
						$$ -> child[0] = $1;
						$$ -> child[1] = $3;
					}
					| factor
					{
						$$ = $1;
					}
					;
mulop				: TIMES
					{ 
						$$ = newExpNode(OpK);
						$$ -> attr.op = TIMES;
					}
					| OVER 
					{ 
						$$ = newExpNode(OpK);
						$$ -> attr.op = OVER;
					}
					;
factor				: LPAREN expression RPAREN
					{
						$$ = $2;
					}
					| var
					{
						$$ = $1;
					}
					| call
					{
						$$ = $1;
					}
					| NUM
					{ 
						$$ = newExpNode(ConstK);
						$$ -> attr.val = atoi(tokenStringTwo);
						$$ -> type = Integer;
					}
					;
call				: id LPAREN args RPAREN
					{ 
						$$ = newStmtNode(CallK);

						//strcpy($$ -> attr.name, tokenString);
						/*strcpy($$ -> attr.name, name[c_name]);
						c_name--;
						$$ -> child[0] = $4;*/
						$$ -> attr.name = $1 -> attr.name;
						$$ -> child[0] = $3;
					} 
					;
args				: arg_list
					{
						//$$ = $1;
						$$ = newExpNode(ArgumentListK);
						$$ -> child[0] = $1;
					}
					| 
					{
						//$$ = $1;
						$$ = NULL;
					}
					;
arg_list			: arg_list COMMA expression
					{
						YYSTYPE t = $1;
						if (t != NULL)
						{
							while (t -> sibling != NULL)
							{
								t=t->sibling;
							}
							t -> sibling = $3;
							$$ = $1;
						}
						else
						{
							$$ = $3;
						}
					}
					| expression
					{
						$$ = $1;
					}
					;
%%

TreeNode * parse(void)
{
//	return (TreeNode*)yyparse();
	yyparse();
	return (TreeNode*)savedTree;
}

int yyerror(char *msg)
{
	if (yychar == 0) return 0;
	
	fprintf(listing, "[ERROR]%s, line number: %d, token: %s\n", msg, lineno, yytext);

//	fprintf(listing, "error at line %d : %s ", lineno, msg);
//	fprintf(listing, " / token: %s\n");
	printToken(yychar, tokenString);
	Error = TRUE;
	return 0;
}
