%{
#define YYPARSER
#include <stdio.h>
#include "globals.h"
#include "util.h"
#include "scan.h"


#define YYSTYPE TreeNode *
static char savedFuncName[100][1024];
static char savedName[100];
static int savedLineNo;
static TreeNode *savedTree;
char tokenString[MAXTOKENLEN+1];
int yyerror(char *message);
TreeNode * parse(void);
extern int c_name;
extern char name[100][1024];
%}

%token IF THEN ELSE WHILE RETURN VOID INT ENDFILE                                                                    
%token NUM ID ERROR
%token SEMI LBRACKET RBRACKET LPAREN RPAREN NE PLUS LT MINUS TIMES OVER EQUAL LE GE EQ COMMA GT LBRACE RBRACE
%left PLUS MINUS
%left TIMES OVER 
%start program 

%%

program				: declaration_list
					{
						savedTree = $1;
					}
					;
declaration_list	: declaration_list declaration
					{
						YYSTYPE t = $1;
						if (t != NULL)
						{
							while (t->sibling != NULL)
							{
								t=t->sibling;
							}
							t->sibling = $2;
							$$ = $1;
						}
						else
						{
							$$ = $2;
						}
					}
					| declaration
					{
						$$ = $1;
					}
					;
declaration			: var_declaration
					{
						$$ = $1;
					}
					| fun_declaration
					{
						$$ = $1;
					}
					;
var_declaration		: type_specifier ID {} SEMI
					{
						$$ = newDecNode(VariableK);
						$$ -> child[0] = $1;
						//$$ -> childIndex = 1;

						//savedLineNo = lineno;
						//strcpy($$ -> attr.name, tokenString);
						strcpy($$ -> attr.name, name[c_name]);

						c_name--;
					}
					| type_specifier ID 
					{
						//savedName = copyString(tokenString);
						//savedLineNo = lineno;
					}
					LBRACKET NUM RBRACKET SEMI
					{
						$$ = newDecNode(ArrK);
						//int i, a = atoi(copyString(tokenString));
						/*for (i=0; i<a; i++)
						{
							TreeNode *t = newDecNode(ArrK);
							t -> lineno = savedLineNo;
							$$ -> child[i] = t;
							$$ -> attr.name = savedName;
						}*/

						$$ -> child[0] = $1;

						$$ -> childIndex = val;
						//strcpy($$ -> attr.name, tokenString);
						strcpy($$->attr.name, name[c_name]);
						c_name--;
						
					}
					;
type_specifier		: INT
					{
						$$ = newDecNode(Integer);
						$$ -> attr.type = INT;
						$$ -> lineno = lineno;
						$$ -> childIndex = 0;
						$$ = newDecNode(TypeK);
						strcpy($$ -> attr.name, "int");
					}
					| VOID
					{
						$$ = newDecNode(Void);
						$$ -> attr.type = VOID;
						$$ -> lineno = lineno;
						$$ -> childIndex = 0;
						$$ = newDecNode(TypeK);
						strcpy($$ -> attr.name, "void");
					}
					;	
fun_declaration		: type_specifier ID
					{
						//savedFuncName = copyString(tokenString);
						//savedLineNo = lineno;
					}
					LPAREN params RPAREN compound_stmt
					{
						$$ = newDecNode(FunctionK);
						//strcpy($$ -> attr.name, savedFuncName);
						//$$ -> childIndex = 3;
						
						$$ -> child[0] = $1;
						strcpy($$ -> attr.name, name[c_name]);
						c_name--;

						$$ -> child[1] = $5;

						$$ -> child[2] = $7;
					}
					;
params				: param_list
					{
						$$ = newDecNode(ParameterDecK);
						$$ -> child[0] = $1;
						//$$ -> childIndex = 1;
					}
					| VOID
					{
						$$ = newDecNode(ParamVoidK);
						//$$ -> childIndex = 0;
					}
					;
param_list			:param_list COMMA param
					{
						YYSTYPE t = $1;
						if (t != NULL)
						{
							while(t -> sibling != NULL)
							{
								t = t-> sibling;
							}
							t -> sibling = $3;
							$$ = $1;
						}
						else
						{
							$$ = $3;
						}
					}
					| param
					{
						$$ = $1;
					}
					;
param				: type_specifier ID
					{
						$$ = newDecNode(ParameterK);
						$$ -> child[0] = $1;
						//$$ -> childIndex = 1;

						//strcpy($$ -> attr.name, tokenString);
						//savedLineNo = lineno;
						strcpy($$ -> attr.name, name[c_name]);
						c_name--;
					}
					| type_specifier ID { } LBRACKET RBRACKET
					{
						$$ = newDecNode(ParameterArrK);
						$$ -> child[0] = $1;

						//strcpy($$ -> attr.name, tokenString);
						//savedLineNo = lineno;
						strcpy($$ -> attr.name, name[c_name]);
						c_name--;
					}
					;
compound_stmt		: LBRACE local_declarations statement_list RBRACE
					{
						$$ = newStmtNode(CompoundK);
						$$ -> child[0] = $2;
						$$ -> child[1] = $3;
						//$$ -> childIndex = 2;
					}
					;
local_declarations	: local_declarations var_declaration
					{
						YYSTYPE t = $1;
						if (t != NULL)
						{
							while(t -> sibling != NULL)
							{
								t = t -> sibling;	
							}
							t -> sibling = $2;
							$$ = $1;
						}
						else
						{
							$$ = $2;
						}
					}
					| empty
					{
						$$ = $1;
					}
					;	
statement_list		: statement_list statement
					{
						YYSTYPE t = $1;
						if (t != NULL)
						{
							while (t -> sibling != NULL)
							{
								t = t -> sibling;
							}
							t -> sibling = $2;
							$$ = $1;
						}
						else
						{
							$$ = $2;
						}
					}
					| empty
					{
						$$ = $1;
					}
					;
statement			: expression_stmt
					{
						$$ = $1;
					}
					| compound_stmt
					{
						$$ = $1;
					}
					| selection_stmt
					{
						$$ = $1;
					}
					| iteration_stmt
					{
						$$ = $1;
					}
					| return_stmt
					{
						$$ = $1;
					}
					;
expression_stmt		: expression SEMI
					{
						$$ = $1;
					}
					| SEMI
					{
						$$ = NULL;
					}
					;
selection_stmt		: IF LPAREN expression RPAREN statement ELSE statement
					{
						$$ = newStmtNode(IfK);
						$$ -> child[0] = $3;
						$$ -> child[1] = $5;
						$6 = newStmtNode(ElseK);
						$$ -> child[2] = $6;
						$$ -> child[3] = $7;
					}
					| IF LPAREN expression RPAREN statement
					{ 
						$$ = newStmtNode(IfK);
						$$ -> child[0] = $3;
						$$ -> child[1] = $5;
					}
					;
iteration_stmt		: WHILE LPAREN expression RPAREN compound_stmt
					{ 
						$$ = newStmtNode(WhileK);
						$$ -> child[0] = $3;
						$$ -> child[1] = $5;
					}
					;
return_stmt			: RETURN SEMI
					{
						$$ = newStmtNode(ReturnK);
					}
					| RETURN expression SEMI
					{  
						$$ = newStmtNode(ReturnK);
						$$ -> child[0] = $2;
					}
					;
expression			: var EQ expression
					{
						$$ = newExpNode(VarK); 
						$$ -> child[0] = $1;
						$$ -> child[1] = newExpNode(OpK);
						$$ -> child[1] -> attr.op = EQ;
						$$ -> child[2] = $3;
					}
					| simple_expression
					{ 
						$$ = newExpNode(SimpleK);
						$$ -> child[0] = $1;
					}

					;
var					: ID
					{
						$$ = newExpNode(IdK);

						//strcpy($$ -> attr.name, tokenString);
						strcpy($$ -> attr.name, name[c_name]);
						c_name--;
					}
					| ID
					{
					
					} LBRACKET expression RBRACKET
					{ 
						$$ = newExpNode(ArrayK);
						$$ -> child[0] = $4;

						//strcpy($$ -> attr.name, tokenString);
						strcpy($$ -> attr.name, name[c_name]);
						c_name--;
					}
					;
simple_expression	: additive_expression relop additive_expression
					{ 
						$$ = newExpNode(SimpleK);		
						$$ -> child[0] = $1;
						$$ -> child[1] = $2;
						$$ -> child[2] = $3;
					}
					| additive_expression
					{ 
						$$ = $1;
					}
					;
relop				: LT
					{  
						$$ = newExpNode(OpK);
						$$ -> attr.op = LT;
					}
					| GT
					{
						$$ = newExpNode(OpK);
						$$ -> attr.op = GT;
					}

					| LE
					{ 
						$$ = newExpNode(OpK);
						$$ -> attr.op = LE;
					}
					| EQUAL
					{ 
						$$ = newExpNode(OpK);
						$$ -> attr.op = EQUAL;
					}
					| NE
					{
						$$ = newExpNode(OpK);
						$$ -> attr.op = NE;
					}
					| GE
					{
						$$ = newExpNode(OpK);
						$$ -> attr.op = GE;
					}
					;
additive_expression	: additive_expression addop term
					{
						$$ = newExpNode(AdditiveK);

						$$ -> child[0] = $1;
						$$ -> child[1] = $2;
						$$ -> child[2] = $3;
					}
					| term
					{
						$$ = $1;
					}
					;
addop				: MINUS
					{  
						$$ = newExpNode(OpK);
						$$ -> attr.op = MINUS;
					}
					| PLUS 
					{ 
						$$ = newExpNode(OpK);
						$$ -> attr.op = PLUS;
					}
					;
term				: term mulop factor
					{
						$$ = newExpNode(TermK);
						$$ -> child[0] = $1;
						$$ -> child[1] = $2;
						$$ -> child[2] = $3;
					}
					| factor
					{
						$$ = $1;
					}
					;
mulop				: TIMES
					{ 
						$$ = newExpNode(OpK);
						$$ -> attr.op = TIMES;
					}
					| OVER 
					{ 
						$$ = newExpNode(OpK);
						$$ -> attr.op = OVER;
					}
					;
factor				: LPAREN expression RPAREN
					{
						$$ = $2;
					}
					| var
					{
						$$ = $1;
					}
					| call
					{
						$$ = $1;
					}
					| NUM
					{ 
						$$ = newExpNode(ConstK);
						//$$ -> attr.val = atoi(tokenString);
						$$ -> attr.val = val;
					}
					;
call				: ID { } LPAREN args RPAREN
					{ 
						$$ = newExpNode(CallK);

						//strcpy($$ -> attr.name, tokenString);
						strcpy($$ -> attr.name, name[c_name]);
						c_name--;
						$$ -> child[0] = $4;
					} 
					;
args				: arg_list
					{
						$$ = $1;
					}
					| empty
					{
						$$ = $1;
					}
					;
arg_list			: arg_list COMMA expression
					{
						YYSTYPE t = $1;
						if (t != NULL)
						{
							while (t -> sibling != NULL)
							{
								t=t->sibling;
							}
							t -> sibling = $3;
							$$ = $1;
						}
						else
						{
							$$ = $3;
						}
					}
					| expression
					{
						$$ = $1;
					}
					;
empty				:
					{
						$$ = NULL;
					}
					;
%%

TreeNode * parse(void)
{
//	return (TreeNode*)yyparse();
	yyparse();
	return (TreeNode*)savedTree;
}

int yyerror(char *msg)
{
	if (yychar == 0) return 0;
	
	fprintf(listing, "[ERROR]%s, line number: %d, token: %s\n", msg, lineno, yytext);

//	fprintf(listing, "error at line %d : %s ", lineno, msg);
//	fprintf(listing, " / token: %s\n");
	printToken(yychar, tokenString);
	Error = TRUE;
	return 0;
}
