/****************************************************/
/* File: symtab.c                                   */
/* Symbol table implementation for the TINY compiler*/
/* (allows only one symbol table)                   */
/* Symbol table is implemented as a chained         */
/* hash table                                       */
/* Compiler Construction: Principles and Practice   */
/* Kenneth C. Louden                                */
/****************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "symtab.h"


/* the hash function */
static int hash ( char * key )
{ int temp = 0;
  int i = 0;
  while (key[i] != '\0')
  { temp = ((temp << SHIFT) + key[i]) % SIZE;
    ++i;
  }
  return temp;
}


/* Procedure st_insert inserts line numbers and
 * memory locations into the symbol table
 * loc = memory location is inserted only the
 * first time, otherwise ignored
 */
void st_insert( char * name, int lineno, int loc, int isFunction, ExpType type, int isDec, int arraySize)
{
	int h = hash(name);
	LOG("			st_insert [%s] -> [%d]\n", name, h); 

	if (rootSymtab == NULL)
	{
		LOG("			> > > > initialize [%s] at st_insert\n", name);
		rootSymtab = (Symtab) malloc(sizeof(struct SymbolTable));
		rootSymtab -> child = NULL;
		rootSymtab -> parent = NULL;
		//strcpy(rootSymtab -> functionName, name);
		rootSymtab -> functionName = name;
		currentSymtab = rootSymtab;
		LOG("					___ASSIGN TO CURRENTSYMTAB 1\n");
	}

	BucketList l = currentSymtab -> hashTable[h];
	while ((l != NULL) && (strcmp(name,l->name) != 0))
	{
		l = l->next;
	}

	Symtab _s = currentSymtab;
	if (l == NULL && isDec != 1)
	{
		while (_s -> parent != NULL)
		{
			_s = _s -> parent;
			l = _s -> hashTable[h];

			while ((l != NULL) && (strcmp(name,l->name) != 0))
				l = l->next;
			
			 if (l != NULL && strcmp(name, l->name) == 0)
				break;
		}
	}

	if (l == NULL) /* variable not yet in table */
	{
		LOG("			add [%s] to table\n", name);
		l = (BucketList) malloc(sizeof(struct BucketListRec));
		l->name = name;
		LOG("					INSERT[%s]\n", name);
		l->lines = (LineList) malloc(sizeof(struct LineListRec));
		l->lines->lineno = lineno;
		l->memloc = loc;
		l->lines->next = NULL;
		l->isFunction = isFunction;
		l->type = type;
		l->next = currentSymtab -> hashTable[h];
		if (type == Array)
		{
			l->arraySize = arraySize;
		}
		else
			l->arraySize = -1;
		currentSymtab -> hashTable[h] = l;
		LOG("					___ASSIGN TO CURRENTSYMTAB 2\n");
	}
	else /* found in table, so just add line number */
	{
		LineList t = l->lines;
		while (t->next != NULL)
			t = t->next;
		t->next = (LineList) malloc(sizeof(struct LineListRec));
		t->next->lineno = lineno;
		t->next->next = NULL;
	}
} /* st_insert */

/* Function st_lookup returns the memory 
 * location of a variable or -1 if not found
 */
int st_lookup ( char *name, TreeNode *t )
{
	int h = hash(name);

	LOG("st_lookup start [%s] [%d]\n", name, h);
	if (rootSymtab == NULL)
	{
		LOG("			> > > > initialize [%s] at st_lookup\n", name);
		rootSymtab = (Symtab) malloc(sizeof(struct SymbolTable));
		currentSymtab = rootSymtab;
		LOG("					___ASSIGN TO CURRENTSYMTAB 4\n");
		
	}
//	LOG("		s1\n");

	searchSymtab = currentSymtab;
//	LOG("		st_lookpup [%s] -> [%d]\n", name, h);

	BucketList l = searchSymtab -> hashTable[h];
	while (TRUE)
	{
//	LOG("		s2\n");

		l = searchSymtab -> hashTable[h];
		LOG("				l is NULL %d\n", l == NULL);
//		LOG("		s2.1\n");
/*		if (l != NULL)
		{
			LOG("	name: [%s]\n", name);
			LOG("		%d", (l -> name)[0]);
			LOG(" %d", (l -> name)[1]);
			LOG(" %d", (l -> name)[2]);
			LOG(" %d\n", (l -> name)[3]);
			LOG("			strlen(l->name): [%d]\n", strlen(l -> name));
			LOG("				l->name: [%s]\n", l -> name);
		}*/
		while (l != NULL && strcmp(name, l -> name) != 0)
		{
			LOG("loop\n");
//			LOG("		s2.2\n");
			l = l -> next;
//			LOG("		s2.3\n");
		}

			
		LOG("before break1\n");
		if (l != NULL && strcmp(name, l -> name) == 0)	
		{
			LOG("break1\n");
//			LOG("		s2.4\n");
			break;
		}
//			LOG("		s3\n");
			LOG("before break2\n");
		if (l == NULL && searchSymtab -> parent == NULL)
		{
			LOG("break2\n");
			break;
		}
		searchSymtab = searchSymtab -> parent;

	}

	if (l == NULL)
		return -1;
	
	t -> type = l -> type;
	t -> isFunction = l -> isFunction;
	return l -> memloc;
//	LOG("lookup end\n");
}

int st_dec_lookup ( char * name, TreeNode *t)
{
	LOG("st_dec_lookup   ");
	int h = hash(name);
	if (rootSymtab == NULL)
	{
		LOG("			> > > > initialize [%s] at st_dec_lookup\n", name);
		rootSymtab = (Symtab)malloc(sizeof(struct SymbolTable));
		rootSymtab -> functionName = name;
		currentSymtab = rootSymtab;
		LOG("					___ASSIGN TO CURRENTSYMTAB 3\n");
	}

	BucketList l = currentSymtab -> hashTable[h];

	while (l != NULL && strcmp(name, l -> name) != 0)
		l = l -> next;
	
	if (l == NULL)
	{
		return -1;
	}
	t -> type = l -> type;
	return l -> memloc;
}

int enterScope(int loc)
{
	/*scope = currentSymtab;
	Symtab _s = (Symtab)malloc(sizeof(struct SymbolTable));

	while (scope -> child != NULL)
		scope = scope -> child;

	_s -> parent = currentSymtab;
	_s -> parentLocation = loc;

	scope -> child = _s;
	currentSymtab = scope -> child;*/


	Symtab _scp = currentSymtab;
	Symtab _s = (Symtab)malloc(sizeof(struct SymbolTable));

	while (_scp -> child != NULL)
		_scp = _scp -> child;

	_s -> parent = currentSymtab;
	_s -> parentLocation = loc;

	_scp -> child = _s;
	currentSymtab = _scp -> child;
	LOG("					___ASSIGN TO CURRENTSYMTAB 5\n");

	return 0;
}

int exitScope(int loc)
{
	if (currentSymtab -> parent != NULL)
	{
		loc = currentSymtab -> parentLocation;
		currentSymtab = currentSymtab -> parent;
		LOG("					___ASSIGN TO CURRENTSYMTAB 6\n");

		return loc;
	}
	return -1;
}

/* Procedure printSymTab prints a formatted 
 * listing of the symbol table contents 
 * to the listing file
 */
void printSymTab(FILE * listing)
{ 
	int i, j;
	j=1;
	int isNotFirst = FALSE;

	rootSymtab = currentSymtab;
	while(rootSymtab->parent!=NULL){
		rootSymtab=rootSymtab->parent;
		j++;
	}

	for (i=0;i<SIZE;++i)
	{
		if (currentSymtab -> hashTable[i] != NULL)
		{
			BucketList l = currentSymtab -> hashTable[i];
			while (l != NULL)
			{

				if (!isNotFirst)
				{
					fprintf(listing,"\nVariable Name Location scope Function     Type     Line Numbers\n");
					fprintf(listing,"------------- -------- ----- -------- ------------ ----------------------\n");
					isNotFirst = TRUE;
				}

				LineList t = l->lines;
				fprintf(listing,"%-13s ",l->name); // name
				fprintf(listing," %-8d ",l->memloc); // location
				fprintf(listing,"%-5d ", j);				
				if(l->isFunction == 1)
					fprintf(listing, " YES     "); //function
				else
					fprintf(listing, " NO      ");

				switch(l->type){
					case 0:
					fprintf(listing, "   VOID     ");break; //Type
					case 1:
					fprintf(listing, "   INT      ");break;
				  case 4:
					if (l->arraySize == -1)
						fprintf(listing, "  ARRAY     ");
					else
						fprintf(listing, "ARRAY[%3d]  ", l->arraySize);
					break;
				  default:
					break;
				}

				if (!isNotFirst)
					return;

				while (t != NULL)
				{
					fprintf(listing,"%-4d ",t->lineno); // line number
					t = t->next;
				}
				fprintf(listing,"\n");
				l = l->next;
			}
		}
	}

	if(currentSymtab->child == NULL)
		return;
	currentSymtab = currentSymtab->child;
	printSymTab(listing);
} /* printSymTab */
