/****************************************************/
/* File: globals.h                                  */
/* Global types and vars for TINY compiler          */
/* must come before other include files             */
/* Compiler Construction: Principles and Practice   */
/* Kenneth C. Louden                                */
/****************************************************/

#ifndef _GLOBALS_H_
#define _GLOBALS_H_

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#ifndef YYPARSER
#include "project3_6.tab.h"
#define ENDFILE 0
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE 1
#endif

#define DEBUG_MODE 0 
#if DEBUG_MODE
	#define LOG printf
#else
	#define LOG(...)
#endif


/* MAXRESERVED = the number of reserved words */
#define MAXRESERVED 8

typedef int TokenType;

//typedef enum 
//{
		/* string token */
//		STRING,
		/* book-keeping tokens */
//		COMMENTERROR, ENDFILE, ERROR,
		/* reserved words */
//		IF, THEN, ELSE, END, REPEAT, UNTIL, READ, WRITE, DO, WHILE, RETURN, VOID, INT, FOR,
		/* multicharacter tokens */
//		ID,NUM,
		/* special symbols */
//		ASSIGN,EQ,LT,PLUS,MINUS,TIMES,OVER,LPAREN,RPAREN,SEMI,NE,LE,GE,COMMA,LBRACE,RBRACE,LBRACKET,RBRACKET,GT
//} TokenType;

extern FILE* source; /* source code text file */
extern FILE* listing; /* listing output text file */
extern FILE* code; /* code text file for TM simulator */

extern int lineno; /* source line number for listing */
extern int yylineno;
extern char * yytext;
int val;
char name[100][1024];
extern int c_name;
/**************************************************/
/***********   Syntax tree for parsing ************/
/**************************************************/

typedef enum {StmtK,ExpK,DecK} NodeKind;
typedef enum {IfK,ElseK,WhileK,CompoundK,ReturnK,AssignK,CallK,CompoundEndK} StmtKind;
//typedef enum {OpK,ConstK,IdK,AddK,AdditiveK,TermK,VarK,SimpleK,ArrayK,FuncK,ParamListK,ArgumentListK} ExpKind;
//typedef enum {ParameterArrK,ArrK,ParamVoidK,ParameterDecK,TypeK,ParameterK,FunctionK,VariableK} DecKind;
typedef enum {OpK, ConstK, IdK, TypeK, ParamListK, ArgumentListK} ExpKind;
typedef enum {ParameterK, FunctionK, VariableK} DecKind;


/* ExpType is used for type checking */
typedef enum {Void,Integer,Boolean,FunctionCall,Array,Default} ExpType;
typedef enum {Void2,Int} DecType;

#define MAXCHILDREN 4 

typedef struct treeNode
   { struct treeNode * child[MAXCHILDREN];
     struct treeNode * sibling;
     int lineno;
     NodeKind nodekind;
     union { StmtKind stmt; ExpKind exp; DecKind dec; } kind;
     union { TokenType op;
             int val;
			 char * name;
	 } attr;
     ExpType type; /* for type checking of exps */
		 DecType typeD;
	 int childIndex;
	 int isFunction;
	 int size;
   } TreeNode;

/**************************************************/
/***********   Flags for tracing       ************/
/**************************************************/

/* EchoSource = TRUE causes the source program to
 * be echoed to the listing file with line numbers
 * during parsing
 */
extern int EchoSource;

/* TraceScan = TRUE causes token information to be
 * printed to the listing file as each token is
 * recognized by the scanner
 */
extern int TraceScan;

/* TraceParse = TRUE causes the syntax tree to be
 * printed to the listing file in linearized form
 * (using indents for children)
 */
extern int TraceParse;

/* TraceAnalyze = TRUE causes symbol table inserts
 * and lookups to be reported to the listing file
 */
extern int TraceAnalyze;

/* TraceCode = TRUE causes comments to be written
 * to the TM code file as code is generated
 */
extern int TraceCode;

/* Error = TRUE prevents further passes if an error occurs */
extern int Error; 
#endif
